/*
 * Copyright 2010-2018 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.danyfel80.registration.surf;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.bioimageanalysis.icy.surf.KeyPoint;
import org.bioimageanalysis.icy.surf.KeyPointDescriptor;
import org.bioimageanalysis.icy.surf.extraction.SurfFeatureCalculator;

import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.Var;
import plugins.kernel.roi.roi2d.ROI2DEllipse;
import plugins.kernel.roi.roi2d.ROI2DLine;

/**
 * This plugin searches for SURF features(Speeded Up Robust Features). These
 * points can be matched afterwards or can be used as descriptors for a specific
 * type of image.
 * 
 * As input, a 2D image must be specified, as well as a Hessian threshold to
 * filter the relevant features. The Hessian threshold is a positive value to
 * truncate the amount of features found to only those with a high enough
 * Hessian value. As the threshold value increases, the amount of features
 * decreases.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class ExtractSurfFeatures extends EzPlug implements Block, EzStoppable {

	EzVarSequence sequenceVar;
	EzVarDouble hessianThresholdVar;

	Var<List<KeyPointDescriptor>> keyPointDescriptorsVar;

	@Override
	public void declareInput(VarList inputMap) {
		sequenceVar = new EzVarSequence("Sequence");
		hessianThresholdVar = new EzVarDouble("Hessian threshold");
		hessianThresholdVar.setValue(1000d);
		inputMap.add(sequenceVar.name, sequenceVar.getVariable());
		inputMap.add(hessianThresholdVar.name, hessianThresholdVar.getVariable());
	}

	@Override
	public void declareOutput(VarList outputMap) {
		keyPointDescriptorsVar = new Var<List<KeyPointDescriptor>>("Features", new ArrayList<>(0));
		outputMap.add(keyPointDescriptorsVar.getName(), keyPointDescriptorsVar);
	}

	@Override
	protected void initialize() {
		sequenceVar = new EzVarSequence("Sequence");
		hessianThresholdVar = new EzVarDouble("Hessian threshold");
		hessianThresholdVar.setValue(1000d);
		addEzComponent(sequenceVar);
		addEzComponent(hessianThresholdVar);
	}

	@Override
	protected void execute() {
		Sequence sequence = sequenceVar.getValue(true);
		BufferedImage image = sequence.getFirstImage();
		double hessianThreshold = hessianThresholdVar.getValue(true);

		SurfFeatureCalculator featureCalculator = new SurfFeatureCalculator();
		featureCalculator.setTargetImage(image);
		image = null;
		featureCalculator.setHessianThreshold(hessianThreshold);
		long startTime, endTime;
		try {
			startTime = System.currentTimeMillis();
			featureCalculator.computeFeatures();
			endTime = System.currentTimeMillis();
			System.out.printf("Minimum hessian value found: %f\n", featureCalculator.getMinHessianValue());
			System.out.printf("Maximum hessian value found: %f\n", featureCalculator.getMaxHessianValue());
			System.out.printf("Number of features found: %d\n", featureCalculator.getKeyPoints().size());
			System.out.printf("Computation time: %d milliseconds", endTime - startTime);
		} catch (InterruptedException e) {
			throw new IcyHandledException("Surf detector interrupted");
		} catch (Exception e) {
			e.printStackTrace();
			throw new IcyHandledException(e);
		}

		if (this.isHeadLess()) {
			keyPointDescriptorsVar.setValue(featureCalculator.getDescriptors());
		} else {
			Sequence result = new Sequence(featureCalculator.getGrayImage());
			result.setName(sequence.getName() + "_SURF");
			result.setPixelSizeX(sequence.getPixelSizeX());
			result.setPixelSizeY(sequence.getPixelSizeY());
			result.setPositionX(sequence.getPositionX());
			result.setPositionY(sequence.getPositionY());
			for (KeyPoint keyPoint : featureCalculator.getKeyPoints()) {
				ROI2DEllipse roi = new ROI2DEllipse(keyPoint.getX() - keyPoint.getScale(),
						keyPoint.getY() - keyPoint.getScale(), keyPoint.getX() + keyPoint.getScale(),
						keyPoint.getY() + keyPoint.getScale());

				double x1 = keyPoint.getX() + keyPoint.getScale() * Math.cos(keyPoint.getOrientation());
				double y1 = keyPoint.getY() + keyPoint.getScale() * Math.sin(keyPoint.getOrientation());
				ROI2DLine roi1 = new ROI2DLine(keyPoint.getX(), keyPoint.getY(), x1, y1);
				result.addROI(roi);
				result.addROI(roi1);
			}
			addSequence(result);
		}
	}

	@Override
	public void clean() {
	}

}
