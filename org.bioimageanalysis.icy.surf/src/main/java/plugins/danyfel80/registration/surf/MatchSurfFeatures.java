package plugins.danyfel80.registration.surf;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.bioimageanalysis.icy.surf.extraction.SurfFeatureCalculator;
import org.bioimageanalysis.icy.surf.matching.DescriptorMatch;
import org.bioimageanalysis.icy.surf.matching.SurfMatcher;
import org.bioimageanalysis.icy.surf.matching.ransac.RansacAffineTransformationCalculator;

import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.util.Random;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DPoint;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class MatchSurfFeatures extends EzPlug implements Block, EzStoppable {

	private EzVarSequence varSequence1;
	private EzVarSequence varSequence2;
	private EzVarDouble varHessianThreshold;
	private EzVarInteger varMatchNumberLimit;
	private VarROIArray varMatchesSequence1;
	private VarROIArray varMatchesSequence2;

	private EzVarInteger varRansacMaxNumIterations;
	private EzVarDouble varRansacMaxErrorThreshold;
	private EzVarInteger varRansacMinNumInliers;

	private long startTime;
	private long endTime;
	private Sequence sequence1;
	private Sequence sequence2;
	private SurfFeatureCalculator featureCalculator1;
	private SurfFeatureCalculator featureCalculator2;
	private SurfMatcher matcher;
	private List<ROI2DPoint> matchesSequence1;
	private List<ROI2DPoint> matchesSequence2;

	@Override
	public void declareInput(VarList inputMap) {
		varSequence1 = new EzVarSequence("Sequence 1");
		varSequence2 = new EzVarSequence("Sequence 2");
		varHessianThreshold = new EzVarDouble("Max hessian illpose threshold");
		varHessianThreshold.setValue(1000d);
		varMatchNumberLimit = new EzVarInteger("Match number limit");
		varMatchNumberLimit.setValue(0);

		varRansacMaxNumIterations = new EzVarInteger("RANSAC max iterations");
		varRansacMaxNumIterations.setValue(RansacAffineTransformationCalculator.DEFAULT_MAX_NUM_ITERATIONS);
		varRansacMaxErrorThreshold = new EzVarDouble("RANSAC max squared error");
		varRansacMaxErrorThreshold.setValue(RansacAffineTransformationCalculator.DEFAULT_SQUARED_ERROR_THRESHOLD);
		varRansacMinNumInliers = new EzVarInteger("RANSAC min inliers");
		varRansacMinNumInliers.setValue(RansacAffineTransformationCalculator.DEFAULT_MIN_NUM_INLIERS);

		inputMap.add(varSequence1.name, varSequence1.getVariable());
		inputMap.add(varSequence2.name, varSequence2.getVariable());
		inputMap.add(varHessianThreshold.name, varHessianThreshold.getVariable());
		inputMap.add(varMatchNumberLimit.name, varMatchNumberLimit.getVariable());

		inputMap.add(varRansacMaxNumIterations.name, varRansacMaxNumIterations.getVariable());
		inputMap.add(varRansacMaxErrorThreshold.name, varRansacMaxErrorThreshold.getVariable());
		inputMap.add(varRansacMinNumInliers.name, varRansacMinNumInliers.getVariable());
	}

	@Override
	public void declareOutput(VarList outputMap) {
		varMatchesSequence1 = new VarROIArray("Match points sequence 1");
		varMatchesSequence2 = new VarROIArray("Match points sequence 2");
		outputMap.add(varMatchesSequence1.getName(), varMatchesSequence1);
		outputMap.add(varMatchesSequence2.getName(), varMatchesSequence2);
	}

	@Override
	protected void initialize() {
		varSequence1 = new EzVarSequence("Sequence 1");
		varSequence2 = new EzVarSequence("Sequence 2");
		varHessianThreshold = new EzVarDouble("Max refining threshold");
		varHessianThreshold.setValue(1000d);
		varHessianThreshold
				.setToolTipText("Maximum hessian location refining threshold. Higher value means fewer resulting points");
		varMatchNumberLimit = new EzVarInteger("Match number limit");
		varMatchNumberLimit.setValue(0);
		varMatchNumberLimit
				.setToolTipText("The max amount of matching points that are found before performing RANSAC filtering");

		varRansacMaxNumIterations = new EzVarInteger("RANSAC max iterations");
		varRansacMaxNumIterations.setValue(RansacAffineTransformationCalculator.DEFAULT_MAX_NUM_ITERATIONS);
		varRansacMaxNumIterations
				.setToolTipText("The maximum number of iterations performed to find an optimal affine transformation.");
		varRansacMaxErrorThreshold = new EzVarDouble("RANSAC max squared error");
		varRansacMaxErrorThreshold.setValue(RansacAffineTransformationCalculator.DEFAULT_SQUARED_ERROR_THRESHOLD);
		varRansacMaxErrorThreshold.setToolTipText(
				"The maximum euclidean squared error between predicted transformation and candidate match positions allowed to consider that match an inlier.");
		varRansacMinNumInliers = new EzVarInteger("RANSAC min inliers");
		varRansacMinNumInliers.setValue(RansacAffineTransformationCalculator.DEFAULT_MIN_NUM_INLIERS);
		varRansacMinNumInliers
				.setToolTipText("The minimum number of inlier matches to consider a candidate affine transformation as valid.");

		addEzComponent(varSequence1);
		addEzComponent(varSequence2);
		addEzComponent(varHessianThreshold);
		addEzComponent(varMatchNumberLimit);

		EzGroup varRansacGroup = new EzGroup("Ransac parameters", varRansacMaxNumIterations, varRansacMaxErrorThreshold,
				varRansacMinNumInliers);
		varRansacGroup.setFoldedState(true);
		varRansacGroup.setToolTipText("Settings associated to the refinement of detected matches using RANSAC");
		addEzComponent(varRansacGroup);
	}

	@Override
	protected void execute() {
		sequence1 = varSequence1.getValue(true);
		sequence2 = varSequence2.getValue(true);
		BufferedImage image1 = sequence1.getFirstImage();
		BufferedImage image2 = sequence2.getFirstImage();
		double hessianThreshold = varHessianThreshold.getValue();
		int matchNumberLimit = varMatchNumberLimit.getValue();
		int ransacMaxNumIterations = varRansacMaxNumIterations.getValue();
		double ransacMaxErrorThreshold = varRansacMaxNumIterations.getValue();
		int ransacMinNumInliers = varRansacMinNumInliers.getValue();

		System.out.println("Finding features for image 1");
		featureCalculator1 = new SurfFeatureCalculator();
		featureCalculator1.setTargetImage(image1);
		featureCalculator1.setHessianThreshold(hessianThreshold);
		try {
			startTime = System.currentTimeMillis();
			featureCalculator1.computeFeatures();
			endTime = System.currentTimeMillis();
			System.out.printf("Minimum hessian value found: %f\n", featureCalculator1.getMinHessianValue());
			System.out.printf("Maximum hessian value found: %f\n", featureCalculator1.getMaxHessianValue());
			System.out.printf("Number of features found: %d\n", featureCalculator1.getKeyPoints().size());
			System.out.printf("Computation time: %d milliseconds\n", endTime - startTime);
		} catch (InterruptedException e) {
			throw getInterruptionException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new IcyHandledException(e);
		}

		System.out.println("Finding features for image 2");
		featureCalculator2 = new SurfFeatureCalculator();
		featureCalculator2.setTargetImage(image2);
		featureCalculator2.setHessianThreshold(hessianThreshold);
		try {
			startTime = System.currentTimeMillis();
			featureCalculator2.computeFeatures();
			endTime = System.currentTimeMillis();
			System.out.printf("Minimum hessian value found: %f\n", featureCalculator2.getMinHessianValue());
			System.out.printf("Maximum hessian value found: %f\n", featureCalculator2.getMaxHessianValue());
			System.out.printf("Number of features found: %d\n", featureCalculator1.getKeyPoints().size());
			System.out.printf("Computation time: %d milliseconds\n", endTime - startTime);
		} catch (InterruptedException e) {
			throw getInterruptionException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new IcyHandledException(e);
		}

		System.out.println("Matching features");
		matcher = new SurfMatcher();
		matcher.setDescriptorList1(featureCalculator1.getDescriptors());
		matcher.setDescriptorList2(featureCalculator2.getDescriptors());
		matcher.setMatchNumberLimit(matchNumberLimit);
		matcher.setRansacMaxNumIterations(ransacMaxNumIterations);
		matcher.setRansacMaxErrorThreshold(ransacMaxErrorThreshold);
		matcher.setRansacMinNumInliers(ransacMinNumInliers);
		try {
			startTime = System.currentTimeMillis();
			matcher.compute();
			endTime = System.currentTimeMillis();
			System.out.printf("Number of matches found: %d\n", matcher.getMatches().size());
			System.out.printf("Computation time: %d milliseconds\n", endTime - startTime);
		} catch (InterruptedException e) {
			throw getInterruptionException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new IcyHandledException(e);
		}

		createResult();

		if (!isHeadLess())
			showResultSequences();
		else
			setOutputPoints();
	}

	private IcyHandledException getInterruptionException() {
		return new IcyHandledException("Feature matching interrupted");
	}

	private void createResult() {
		matchesSequence1 = new ArrayList<>(matcher.getMatches().size());
		matchesSequence2 = new ArrayList<>(matcher.getMatches().size());
		int num = 1;
		for (DescriptorMatch match: matcher.getMatches()) {
			ROI2DPoint roi1 = new ROI2DPoint(match.getX1(), match.getY1());
			ROI2DPoint roi2 = new ROI2DPoint(match.getX2(), match.getY2());
			roi1.setName("" + num);
			roi2.setName("" + num);
			Color color = new Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256));
			roi1.setColor(color);
			roi2.setColor(color);
			roi1.setShowName(true);
			roi2.setShowName(true);

			matchesSequence1.add(roi1);
			matchesSequence2.add(roi2);
			num++;
		}
	}

	private void showResultSequences() {
		Sequence s1 = new Sequence(featureCalculator1.getGrayImage());
		s1.setName(sequence1.getName() + "_SURF");
		s1.setPixelSizeX(sequence1.getPixelSizeX());
		s1.setPixelSizeY(sequence1.getPixelSizeY());
		s1.setPositionX(sequence1.getPositionX());
		s1.setPositionY(sequence1.getPositionY());
		s1.addROIs(matchesSequence1, false);
		addSequence(s1);

		Sequence s2 = new Sequence(featureCalculator2.getGrayImage());
		s2.setName(sequence2.getName() + "_SURF");
		s2.setPixelSizeX(sequence2.getPixelSizeX());
		s2.setPixelSizeY(sequence2.getPixelSizeY());
		s2.setPositionX(sequence2.getPositionX());
		s2.setPositionY(sequence2.getPositionY());
		s2.addROIs(matchesSequence2, false);
		addSequence(s2);
	}

	private void setOutputPoints() {
		varMatchesSequence1.setValue(matchesSequence1.toArray(new ROI[matchesSequence1.size()]));
		varMatchesSequence2.setValue(matchesSequence2.toArray(new ROI[matchesSequence2.size()]));
	}

	@Override
	public void clean() {}

}
