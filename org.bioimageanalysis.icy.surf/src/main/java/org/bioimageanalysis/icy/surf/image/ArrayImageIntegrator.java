package org.bioimageanalysis.icy.surf.image;

import com.google.common.util.concurrent.AtomicDouble;

public class ArrayImageIntegrator {
	private ArrayImage targetImage;
	private int padding = -1;

	private ArrayImage paddedImage;
	private int paddedWidth;
	private int paddedHeight;

	private ArrayImage integralImage;

	public ArrayImageIntegrator() {
	}

	public int getPadding() {
		return padding;
	}

	public void setPaddingSize(int padding) {
		this.padding = padding;
	}

	private ArrayImage getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(ArrayImage image) {
		this.targetImage = image;
	}

	private void unsetTargetImage() {
		this.targetImage = null;
	}

	public synchronized void compute() {
		if (getTargetImage() == null)
			throw new RuntimeException("No target image specified");
		if (getPadding() < 0)
			throw new RuntimeException("No padding specified");

		ArrayImage paddedImage = padImage(getTargetImage(), getPadding());
		unsetTargetImage();
		setPaddedImage(paddedImage);

		integralImage = new ArrayImage(paddedWidth, paddedHeight);
		computeIntegral();
		unsetPaddedImage();
	}

	private ArrayImage padImage(ArrayImage image, int padding) {
		ArrayImagePadder padder = new ArrayImagePadder();
		padder.setTargetImage(image);
		padder.setPaddingSize(padding);
		padder.compute();
		return padder.getPaddedImage();
	}

	private void setPaddedImage(ArrayImage paddedImage) {
		this.paddedImage = paddedImage;
		this.paddedWidth = paddedImage.getWidth();
		this.paddedHeight = paddedImage.getHeight();
	}

	private void unsetPaddedImage() {
		this.paddedImage = null;
	}

	private ArrayImage getPaddedImage() {
		return this.paddedImage;
	}

	private void computeIntegral() {
		integralImage.getDataArray()[0][0] = 0;
		for (int x = 1; x < paddedWidth; x++) {
			computeIntegralFirstRowPixelValues(x);
		}

		for (int y = 1; y < paddedHeight; y++) {
			computeLineIntegralPixelValues(y);
		}
	}

	private void computeIntegralFirstRowPixelValues(int x) {
		integralImage.setValue(x, 0, integralImage.getValueAt(x - 1, 0) + getPaddedImage().getValueAt(x, 0));
	}

	private void computeLineIntegralPixelValues(int y) {
		AtomicDouble lineSum = new AtomicDouble();
		for (int x = 0; x < paddedWidth; x++) {
			computeIntegralPixelValues(x, y, lineSum);
		}
	}

	private void computeIntegralPixelValues(int x, int y, AtomicDouble lineSum) {
		lineSum.addAndGet(getPaddedImage().getDataArray()[y][x]);
		integralImage.setValue(x, y, lineSum.get() + integralImage.getValueAt(x, y - 1));
	}

	public ArrayImage getIntegralImage() {
		return this.integralImage;
	}

	public double getSquareConvolutionXYAt(int x, int y, int boundX0, int boundX1, int boundY0, int boundY1) {
		int a1 = x - boundX0;
		int a2 = y - boundX1;
		int b1 = a1 - boundY0;
		int b2 = a2 - boundY1;

		a1 += padding;
		a2 += padding;
		b1 += padding;
		b2 += padding;

		double result = 0;

		try {
			result += integralImage.getValueAt(b1, b2);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d)", b1, b2));
		}
		try {
			result += integralImage.getValueAt(a1, a2);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d)", a1, a2));
		}
		try {
			result -= integralImage.getValueAt(b1, a2);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d)", b1, a2));
		}

		try {
			result -= integralImage.getValueAt(a1, b2);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d)", a1, b2));
		}

		return result;
	}

	public double getHaarX(int x, int y, int lambda) {
		double result = -getSquareConvolutionXYAt(x, y, 1, -lambda - 1, -lambda - 1, 2 * lambda + 1);
		result -= getSquareConvolutionXYAt(x, y, 0, -lambda - 1, lambda + 1, 2 * lambda + 1);
		return result;
	}

	public double getHaarY(int x, int y, int lambda) {
		double result = 0;
		result -= getSquareConvolutionXYAt(x, y, -lambda - 1, 1, 2 * lambda + 1, -lambda - 1);
		result -= getSquareConvolutionXYAt(x, y, -lambda - 1, 0, 2 * lambda + 1, lambda + 1);
		return result;
	}
}
