package org.bioimageanalysis.icy.surf;

public class KeyPointSectorDescriptor {
	private double sumDx;
	private double sumDy;
	private double sumAbsDx;
	private double sumAbsDy;

	public KeyPointSectorDescriptor() {}

	public KeyPointSectorDescriptor(double sumDx, double sumDy, double sumAbsDx, double sumAbsDy) {
		this.sumDx = sumDx;
		this.sumDy = sumDy;
		this.sumAbsDx = sumAbsDx;
		this.sumAbsDy = sumAbsDy;
	}

	public double getSumDx() {
		return sumDx;
	}

	public void setSumDx(double sumDx) {
		this.sumDx = sumDx;
	}

	public void addToSumDx(double value) {
		sumDx += value;
	}

	public void divideSumDxBy(double value) {
		sumDx /= value;
	}

	public double getSumDy() {
		return sumDy;
	}

	public void setSumDy(double sumDy) {
		this.sumDy = sumDy;
	}

	public void addToSumDy(double value) {
		sumDy += value;
	}

	public void divideSumDyBy(double value) {
		sumDy /= value;
	}

	public double getSumAbsDx() {
		return sumAbsDx;
	}

	public void setSumAbsDx(double sumAbsDx) {
		this.sumAbsDx = sumAbsDx;
	}

	public void addToSumAbsDx(double value) {
		sumAbsDx += value;
	}

	public void divideSumAbsDxBy(double value) {
		sumAbsDx /= value;
	}

	public double getSumAbsDy() {
		return sumAbsDy;
	}

	public void setSumAbsDy(double sumAbsDy) {
		this.sumAbsDy = sumAbsDy;
	}

	public void addToSumAbsDy(double value) {
		sumAbsDy += value;
	}

	public void divideSumAbsDyBy(double value) {
		sumAbsDy /= value;
	}

}
