package org.bioimageanalysis.icy.surf;

public class KeyPoint {
	private double x, y;
	private double scale;
	private double orientation;
	private boolean laplacianSignPositive;

	public KeyPoint(double x, double y, double scale, double orientation, boolean laplacianSign) {
		this.x = x;
		this.y = y;
		this.scale = scale;
		this.orientation = orientation;
		this.laplacianSignPositive = laplacianSign;
	}

	public KeyPoint(KeyPoint keyPoint) {
		this.x = keyPoint.getX();
		this.y = keyPoint.getY();
		this.scale = keyPoint.getScale();
		this.orientation = keyPoint.getOrientation();
		this.laplacianSignPositive = keyPoint.isLaplacianSignPositive();
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getScale() {
		return scale;
	}

	public double getOrientation() {
		return orientation;
	}

	public boolean isLaplacianSignPositive() {
		return laplacianSignPositive;
	}

	@Override
	public String toString() {
		return String.format("Key point [x=%f, y=%f, scale=%f, orientation=%f, laplacianSignPositive=%b]", x, y, scale, orientation,
				laplacianSignPositive);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (laplacianSignPositive ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(orientation);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(scale);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof KeyPoint)) {
			return false;
		}
		KeyPoint other = (KeyPoint) obj;
		if (laplacianSignPositive != other.laplacianSignPositive) {
			return false;
		}
		if (Double.doubleToLongBits(orientation) != Double.doubleToLongBits(other.orientation)) {
			return false;
		}
		if (Double.doubleToLongBits(scale) != Double.doubleToLongBits(other.scale)) {
			return false;
		}
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		return true;
	}
	
	
}
