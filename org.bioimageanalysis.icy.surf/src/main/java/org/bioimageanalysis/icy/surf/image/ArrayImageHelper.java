package org.bioimageanalysis.icy.surf.image;

import icy.image.IcyBufferedImage;
import icy.type.DataType;

public abstract class ArrayImageHelper {

	/**
	 * @param bufferedImage
	 *          Single channel BufferedImage.
	 * @return Single channel image.
	 */
	public static ArrayImage getArrayImageFrom(IcyBufferedImage bufferedImage) {
		if (bufferedImage.getSizeC() > 1)
			throw new IllegalArgumentException("Buffered image has more than 1 channels.");

		ArrayImage image = new ArrayImage(bufferedImage.getWidth(), bufferedImage.getHeight());

		double[] xyDataArray = bufferedImage.getDataXYAsDouble(0);
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				image.setValue(x, y, xyDataArray[y * bufferedImage.getWidth() + x]);
			}
		}
		return image;
	}

	/**
	 * @param bufferedImage
	 *          Multi-channel BufferedImage.
	 * @return Array of single channel images. One ArrayImage per buffered image
	 *         channel/
	 */
	public static ArrayImage[] getArrayImagesFrom(IcyBufferedImage bufferedImage) {

		ArrayImage[] images = new ArrayImage[bufferedImage.getSizeC()];
		for (int c = 0; c < bufferedImage.getSizeC(); c++) {
			images[c] = new ArrayImage(bufferedImage.getWidth(), bufferedImage.getHeight());

			double[] xyDataArray = bufferedImage.getDataXYAsDouble(c);
			for (int x = 0; x < images[c].getWidth(); x++) {
				for (int y = 0; y < images[c].getHeight(); y++) {
					images[c].setValue(x, y, xyDataArray[y * bufferedImage.getWidth() + x]);
				}
			}
		}
		return images;
	}

	/**
	 * @param image
	 *          Array image to convert.
	 * @return Converted image.
	 */
	public static IcyBufferedImage getBufferedImageFrom(ArrayImage image) {
		IcyBufferedImage bImage = new IcyBufferedImage(image.getWidth(), image.getHeight(), 1, DataType.DOUBLE);

		bImage.beginUpdate();
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				bImage.setData(x, y, 0, image.getValueAt(x, y));
			}
		}
		bImage.endUpdate();

		return bImage;
	}

	/**
	 * @param imageArray
	 *          Array of ArrayImages to convert.
	 * @return Single converted image.
	 */
	public static IcyBufferedImage getBufferedImageFrom(ArrayImage[] imageArray) {
		IcyBufferedImage bImage = new IcyBufferedImage(imageArray[0].getWidth(), imageArray[0].getHeight(),
				imageArray.length, DataType.DOUBLE);

		bImage.beginUpdate();
		for (int c = 0; c < imageArray.length; c++) {
			for (int x = 0; x < imageArray[0].getWidth(); x++) {
				for (int y = 0; y < imageArray[0].getHeight(); y++) {
					bImage.setData(x, y, c, imageArray[c].getValueAt(x, y));
				}
			}
		}
		bImage.endUpdate();

		return bImage;
	}
	
	public static int getImageWidthForSamplingStep(IcyBufferedImage image, int currentSamplingStep) {
		return image.getWidth()/currentSamplingStep;
	}

	public static int getImageHeightForSamplingStep(IcyBufferedImage image, int currentSamplingStep) {
		return image.getHeight()/currentSamplingStep;
	}
}
