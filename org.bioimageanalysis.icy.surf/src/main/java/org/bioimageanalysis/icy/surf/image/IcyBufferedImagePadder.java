package org.bioimageanalysis.icy.surf.image;

import icy.image.IcyBufferedImage;

public class IcyBufferedImagePadder {

	private int targetWidth;
	private int targetHeight;
	private int targetSizeC;
	private IcyBufferedImage targetImage;

	private int padding;

	private int paddedWidth;
	private int paddedHeight;
	private IcyBufferedImage paddedImage;

	public IcyBufferedImagePadder() {}

	public IcyBufferedImage getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(IcyBufferedImage image) {
		this.targetImage = image;
		this.targetWidth = targetImage.getWidth();
		this.targetHeight = targetImage.getHeight();
		this.targetSizeC = targetImage.getSizeC();
	}
	
	private void unsetTargetImage() {
		this.targetImage = null;
		this.targetWidth = -1;
		this.targetHeight = -1;
		this.targetSizeC = -1;
	}
	
	public int getPaddingSize() {
		return this.padding;
	}
	
	public void setPaddingSize(int padding) {
		this.padding = padding;
	}
	
	private void unsetPaddingSize() {
		this.padding = -1;
	}

	public synchronized void compute() {
		if (getTargetImage() == null) throw new RuntimeException("No target image specified");
		if (getPaddingSize() < 0) throw new RuntimeException("No padding size specified");
		
		paddedWidth = targetWidth + 2 * padding;
		paddedHeight = targetHeight + 2 * padding;
		paddedImage = new IcyBufferedImage(paddedWidth, paddedHeight, targetSizeC, getTargetImage().getDataType_());
		paddedImage.beginUpdate();
		for (int c = 0; c < targetSizeC; c++) {
			fillPaddedImageChannel(c);
		}
		paddedImage.endUpdate();

		unsetTargetImage();
		unsetPaddingSize();
	}

	private void fillPaddedImageChannel(int c) {
		int targetYPosition;
		for (int paddedYPosition = 0; paddedYPosition < paddedHeight; paddedYPosition++) {
			targetYPosition = getYPositionAtTargetImage(paddedYPosition);
			fillPaddedImageLine(c, paddedYPosition, targetYPosition);
		}
	}

	private int getYPositionAtTargetImage(int paddedYPosition) {
		if (paddedYPosition < padding)
			return getMirroredPositionAtTargetImage(padding - paddedYPosition, targetHeight);
		else if (paddedYPosition < paddedHeight - padding)
			return paddedYPosition - padding;
		else
			return getMirroredPositionAtTargetImage((targetHeight + paddedHeight - padding - 2) - paddedYPosition,
					targetHeight);
	}

	private int getMirroredPositionAtTargetImage(int position, int length) {
		if (position < 0)
			return getMirroredPositionAtTargetImage(-position, length);
		else if (position < length)
			return position;
		else
			return getMirroredPositionAtTargetImage((2 * length) - 2 - position, length);
	}

	private void fillPaddedImageLine(int c, int paddedYPosition, int targetYPosition) {
		int targetXPosition;
		for (int paddedXPosition = 0; paddedXPosition < paddedWidth; paddedXPosition++) {
			targetXPosition = getXPositionAtTargetImage(paddedXPosition);
			fillPaddedImagePixel(c, paddedXPosition, paddedYPosition, targetXPosition, targetYPosition);
		}
	}

	private int getXPositionAtTargetImage(int paddedXPosition) {
		if (paddedXPosition < padding)
			return getMirroredPositionAtTargetImage(padding - paddedXPosition, targetWidth);
		else if (paddedXPosition < paddedWidth - padding)
			return paddedXPosition - padding;
		else
			return getMirroredPositionAtTargetImage((targetWidth + paddedWidth - padding - 2) - paddedXPosition, targetWidth);
	}

	private void fillPaddedImagePixel(int c, int paddedXPosition, int paddedYPosition, int targetXPosition,
			int targetYPosition) {
		double value;
		try {
			value = getTargetImage().getData(targetXPosition, targetYPosition, c);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(
					String.format("Invalid target image position (%d, %d)", targetXPosition, targetYPosition));
		}
		try {
			paddedImage.setData(paddedXPosition, paddedYPosition, c, value);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(
					String.format("Invalid padded image position (%d, %d)", paddedXPosition, paddedYPosition));
		}
	}

	public IcyBufferedImage getPaddedImage() {
		return paddedImage;
	}
}
