package org.bioimageanalysis.icy.surf.matching.ransac;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.bioimageanalysis.icy.surf.matching.DescriptorMatch;

public class RansacAffineTransformationCalculator {
	public static final double DEFAULT_SQUARED_ERROR_THRESHOLD = 0.8d;
	public static final int DEFAULT_MIN_NUM_INLIERS = 6;
	public static final int DEFAULT_MAX_NUM_ITERATIONS = 30;

	public static class Builder {
		private List<DescriptorMatch> matches;
		private double errorThreshold = DEFAULT_SQUARED_ERROR_THRESHOLD;
		private int numInliersThreshold = DEFAULT_MIN_NUM_INLIERS;
		private int maxNumIterations = DEFAULT_MAX_NUM_ITERATIONS;

		/**
		 * Creates a builder with defaults:
		 * <ul>
		 * <li>Error threshold = 0.8</li>
		 * <li>Min num inliers threshold = 6</li>
		 * <li>Max num iterations = 30</li>
		 * </ul>
		 * 
		 * @param matches
		 *          Matching points.
		 */
		public Builder(List<DescriptorMatch> matches) {
			if (matches == null)
				throw new IllegalArgumentException("Null matches");
			if (matches.isEmpty())
				throw new IllegalArgumentException("Empty matches");
			this.matches = matches;
		}

		public Builder setErrorThreshold(double error) {
			this.errorThreshold = error;
			return this;
		}

		public Builder setNumInliersThreshold(int minNumInliers) {
			this.numInliersThreshold = minNumInliers;
			return this;
		}

		public Builder setMaxNumIterations(int numIterations) {
			this.maxNumIterations = numIterations;
			return this;
		}

		public RansacAffineTransformationCalculator build() {
			RansacAffineTransformationCalculator calculator = new RansacAffineTransformationCalculator();
			calculator.setMatches(matches);
			calculator.setErrorThreshold(errorThreshold);
			calculator.setNumInliersThreshold(numInliersThreshold);
			calculator.setMatNumIterations(maxNumIterations);
			return calculator;
		}
	}

	// Based on sample number estimation from http://www.cse.psu.edu/~rtc12/CSE486/lecture15.pdf
	private static final int MIN_INPUT_MATCHES = 6;

	private List<DescriptorMatch> matches;
	private double errorThreshold;
	private int numInliersThreshold;
	private int maxNumIterations;

	private RansacAffineTransformationCalculator() {}

	private void setMatches(List<DescriptorMatch> matches) {
		this.matches = matches;
		this.transformedPoints = new ArrayList<>(matches.size());
		for (@SuppressWarnings("unused")
		DescriptorMatch descriptor: matches) {
			this.transformedPoints.add(new Point2D.Double());
		}
	}

	private void setErrorThreshold(double errorThreshold) {
		this.errorThreshold = errorThreshold;
	}

	private void setNumInliersThreshold(int numInliersThreshold) {
		this.numInliersThreshold = numInliersThreshold;
	}

	private void setMatNumIterations(int maxNumIterations) {
		this.maxNumIterations = maxNumIterations;
	}

	private double bestModelError;
	private ArrayRealVector bestModelParameters;
	private List<Integer> bestInliers;

	private RealVector currentTransformationParameters;
	private List<Integer> currentInliers = new LinkedList<>();
	private List<Double> currentInlierErrors = new LinkedList<>();
	private double currentModelError;

	/**
	 * @throws RansacAffineTransformationCalculatorException
	 *           If not enough matches are present to compute the RANSAC
	 *           procedure.
	 */
	public void compute() throws RansacAffineTransformationCalculatorException {
		bestModelError = Double.POSITIVE_INFINITY;
		bestModelParameters = null;
		bestInliers = null;

		int iteration = 0;
		while (iteration < maxNumIterations) {
			iteration++;
			try {
				findNewHypothesisTransformation();
			} catch (RansacAffineTransformationCalculatorException e) {
				if (e.getCause() instanceof SingularMatrixException) {
					continue; // try finding another hypothesis with non singular solution
				} else {
					throw e;
				}
			}
			computeTransformedMatchPoints();
			findCurrentInliers();
			if (currentInliers.size() > numInliersThreshold) {
				computeCurrentModelErrorOnInliers();
				if (currentModelError < bestModelError) {
					bestModelError = currentModelError;
					bestModelParameters = new ArrayRealVector(currentTransformationParameters);
					bestInliers = new ArrayList<>(currentInliers);
				}
			}
		}
		System.out.println("RansacAffineTransformationCalculator: Total iterations = " + iteration);
		if (bestModelParameters == null) {
			throw new RansacAffineTransformationCalculatorException(
					"Could not find a transformation with more than the minimum amount of inliers");
		}
	}

	private DescriptorMatch[] hypothesisMatches = new DescriptorMatch[3];
	private RealMatrix matrixX = MatrixUtils.createRealMatrix(6, 6);
	private RealVector vectorXP = new ArrayRealVector(6);

	private void findNewHypothesisTransformation() throws RansacAffineTransformationCalculatorException {
		pick3RandomMatches();
		buildMatrixX();
		buildVectorXP();
		solveTransformationParameters();
	}

	private Random randomGenerator = new Random();

	private void pick3RandomMatches() throws RansacAffineTransformationCalculatorException {
		if (matches.size() < MIN_INPUT_MATCHES) {
			throw new RansacAffineTransformationCalculatorException("Not enough matches to compute RANSAC filter");
		}
		hypothesisMatches[0] = matches.get(randomGenerator.nextInt(matches.size()));
		hypothesisMatches[1] = matches.get(randomGenerator.nextInt(matches.size()));
		hypothesisMatches[2] = matches.get(randomGenerator.nextInt(matches.size()));
	}

	private void buildMatrixX() {
		matrixX.setEntry(0, 0, hypothesisMatches[0].getX1());
		matrixX.setEntry(0, 1, hypothesisMatches[0].getY1());
		matrixX.setEntry(0, 2, 1d);
		matrixX.setEntry(1, 3, hypothesisMatches[0].getX1());
		matrixX.setEntry(1, 4, hypothesisMatches[0].getY1());
		matrixX.setEntry(1, 5, 1d);
		matrixX.setEntry(2, 0, hypothesisMatches[1].getX1());
		matrixX.setEntry(2, 1, hypothesisMatches[1].getY1());
		matrixX.setEntry(2, 2, 1d);
		matrixX.setEntry(3, 3, hypothesisMatches[1].getX1());
		matrixX.setEntry(3, 4, hypothesisMatches[1].getY1());
		matrixX.setEntry(3, 5, 1d);
		matrixX.setEntry(4, 0, hypothesisMatches[2].getX1());
		matrixX.setEntry(4, 1, hypothesisMatches[2].getY1());
		matrixX.setEntry(4, 2, 1d);
		matrixX.setEntry(5, 3, hypothesisMatches[2].getX1());
		matrixX.setEntry(5, 4, hypothesisMatches[2].getY1());
		matrixX.setEntry(5, 5, 1d);
	}

	private void buildVectorXP() {
		vectorXP.setEntry(0, hypothesisMatches[0].getX2());
		vectorXP.setEntry(1, hypothesisMatches[0].getY2());
		vectorXP.setEntry(2, hypothesisMatches[1].getX2());
		vectorXP.setEntry(3, hypothesisMatches[1].getY2());
		vectorXP.setEntry(4, hypothesisMatches[2].getX2());
		vectorXP.setEntry(5, hypothesisMatches[2].getY2());
	}

	private void solveTransformationParameters() throws RansacAffineTransformationCalculatorException {
		DecompositionSolver solver = new QRDecomposition(matrixX).getSolver();
		try {
			currentTransformationParameters = solver.solve(vectorXP);
		} catch (SingularMatrixException e) {
			throw new RansacAffineTransformationCalculatorException("Could not find a non singular decomposed matrix.", e);
		}
	}

	private List<Point2D> transformedPoints;

	private void computeTransformedMatchPoints() {
		DescriptorMatch match;
		for (int i = 0; i < matches.size(); i++) {
			match = matches.get(i);
			double xT = currentTransformationParameters.getEntry(0) * match.getX1()
					+ currentTransformationParameters.getEntry(1) * match.getY1() + currentTransformationParameters.getEntry(2);
			double yT = currentTransformationParameters.getEntry(3) * match.getX1()
					+ currentTransformationParameters.getEntry(4) * match.getY1() + currentTransformationParameters.getEntry(5);
			transformedPoints.get(i).setLocation(xT, yT);
		}
	}

	private double currentMatchError;

	private void findCurrentInliers() {
		currentInliers.clear();
		for (int i = 0; i < matches.size(); i++) {
			computeCurrentMatchError(matches.get(i), transformedPoints.get(i));
			if (currentMatchError < errorThreshold) {
				currentInliers.add(i);
				currentInlierErrors.add(currentMatchError);
			}
		}
	}

	private double errorX, errorY;

	private void computeCurrentMatchError(DescriptorMatch descriptorMatch, Point2D transformedPoint) {
		errorX = descriptorMatch.getX2() - transformedPoint.getX();
		errorY = descriptorMatch.getY2() - transformedPoint.getY();
		currentMatchError = errorX * errorX + errorY * errorY;
	}

	private void computeCurrentModelErrorOnInliers() {
		currentModelError = currentInlierErrors.stream().mapToDouble(v -> v).sum();
	}

	public RealVector getBestAffineTransformationParameters() {
		return bestModelParameters;
	}

	public List<DescriptorMatch> getFilteredMatches() {
		List<DescriptorMatch> filteredMatches = new ArrayList<>(bestInliers.size());
		for (int i = 0; i < bestInliers.size(); i++) {
			filteredMatches.add(matches.get(bestInliers.get(i)));
		}
		return filteredMatches;
	}
}
