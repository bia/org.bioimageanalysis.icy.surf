package org.bioimageanalysis.icy.surf.image;

public class ArrayImage {

	private int width;
	private int height;
	private double[][] dataArray;

	public ArrayImage(int width, int height) {
		this.width = width;
		this.height = height;
		this.dataArray = new double[height][width];
	}

	public double[][] getDataArray() {
		return dataArray;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public double getValueAt(int x, int y) {
		return dataArray[y][x];
	}

	public void setValue(int x, int y, double value) {
		getDataArray()[y][x] = value;
	}
}
