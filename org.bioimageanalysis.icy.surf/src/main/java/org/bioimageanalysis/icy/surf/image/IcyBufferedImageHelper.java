package org.bioimageanalysis.icy.surf.image;

import icy.image.IcyBufferedImage;

public abstract class IcyBufferedImageHelper {

	public static int getImageWidthForSamplingStep(IcyBufferedImage image, int currentSamplingStep) {
		return image.getWidth()/currentSamplingStep;
	}

	public static int getImageHeightForSamplingStep(IcyBufferedImage image, int currentSamplingStep) {
		return image.getHeight()/currentSamplingStep;
	}
}
