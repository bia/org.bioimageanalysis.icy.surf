package org.bioimageanalysis.icy.surf;

import java.util.ArrayList;
import java.util.List;

import org.bioimageanalysis.icy.surf.image.ArrayImageIntegrator;

public class KeyPointDescriptor {

	private static final int DESCRIPTOR_SECTOR_SIZE_1D = 4;
	private static final int DESCRIPTOR_SECTOR_GRID_SIZE_2D = DESCRIPTOR_SECTOR_SIZE_1D * DESCRIPTOR_SECTOR_SIZE_1D;

	public static List<KeyPointDescriptor> createDescriptors(ArrayImageIntegrator integrator,
			List<KeyPoint> keyPoints) {
		List<KeyPointDescriptor> descriptors = new ArrayList<>(keyPoints.size());
		for (KeyPoint keyPoint : keyPoints) {
			descriptors.add(createKeyPointDescriptor(integrator, keyPoint));
		}
		return descriptors;
	}

	private static KeyPointDescriptor createKeyPointDescriptor(ArrayImageIntegrator integrator, KeyPoint keyPoint) {
		KeyPointDescriptor descriptor = new KeyPointDescriptor();

		// Divide in a 4x4 zone the space around the interest point
		// First compute the orientation.
		double cosP = Math.cos(keyPoint.getOrientation());
		double sinP = Math.sin(keyPoint.getOrientation());
		double norm = 0;

		// Divide in 16 sectors the space around the interest point.
		for (int i = 0; i < DESCRIPTOR_SECTOR_SIZE_1D; i++) {
			for (int j = 0; j < DESCRIPTOR_SECTOR_SIZE_1D; j++) {
				computeSectorDescriptorInDescriptor(integrator, keyPoint, cosP, sinP, descriptor, i, j);

				// Compute the norm of the vector
				norm += computeSectorDescriptorsNorm(descriptor, i, j);
			}
		}
		// Normalization of the descriptors in order to improve invariance to
		// contrast change and whitening the descriptors.
		norm = Math.sqrt(norm);
		if (norm != 0)
			normalizeSectorDescriptorsBy(descriptor, norm);

		descriptor.setKeyPoint(keyPoint);
		return descriptor;
	}

	private static void computeSectorDescriptorInDescriptor(ArrayImageIntegrator integrator, KeyPoint keyPoint,
			double cosP, double sinP, KeyPointDescriptor descriptor, int i, int j) {
		double u, v, gauss, responseU, responseV, responseX, responseY;
		descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).setSumDx(0);
		descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).setSumAbsDx(0);
		descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).setSumDy(0);
		descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).setSumAbsDy(0);
		// Then each 4x4 is subsampled into a 5x5 zone
		for (int k = 0; k < 5; k++) {
			for (int l = 0; l < 5; l++) {
				// We pre compute Haar answers
				u = (keyPoint.getX() + keyPoint.getScale() * (cosP * ((i - 2) * 5 + k + 0.5) - sinP * ((j - 2) * 5 + l + 0.5)));
				v = (keyPoint.getY() + keyPoint.getScale() * (sinP * ((i - 2) * 5 + k + 0.5) + cosP * ((j - 2) * 5 + l + 0.5)));
				responseX = integrator.getHaarX((int) u, (int) v, (int) Math.round(keyPoint.getScale()));
				// (u,v) are already translated of 0.5, which means
				// that there is no round-off to perform: one takes
				// the integer part of the coordinates.
				responseY = integrator.getHaarY((int) u, (int) v, (int) Math.round(keyPoint.getScale()));

				// Gaussian weight
				gauss = getGaussian(((i - 2) * 5 + k + 0.5), ((j - 2) * 5 + l + 0.5), 3.3);

				// Rotation of the axis
				// responseU = gauss*( -responseX*sinP + responseY*cosP);
				// responseV = gauss*(responseX*cosP + responseY*sinP);
				responseU = gauss * (responseX * cosP + responseY * sinP);
				responseV = gauss * (-responseX * sinP + responseY * cosP);

				// The descriptors.
				descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).addToSumDx(responseU);
				descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).addToSumAbsDx(Math.abs(responseU));
				descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).addToSumDy(responseV);
				descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).addToSumAbsDy(Math.abs(responseV));

			}
		}
	}

	public static double getGaussian(double x, double y, double sigma) {
		return 1 / (2 * Math.PI * sigma * sigma) * Math.exp(-(x * x + y * y) / (2 * sigma * sigma));
	}

	private static double computeSectorDescriptorsNorm(KeyPointDescriptor descriptor, int i, int j) {
		return descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumAbsDx()
				* descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumAbsDx()
				+ descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumAbsDy()
						* descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumAbsDy()
				+ descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumDx()
						* descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumDx()
				+ descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumDy()
						* descriptor.getSectorDescriptor(DESCRIPTOR_SECTOR_SIZE_1D * i + j).getSumDy();
	}

	private static void normalizeSectorDescriptorsBy(KeyPointDescriptor descriptor, double norm) {
		for (int i = 0; i < descriptor.getSectorDescriptors().length; i++) {
			(descriptor.getSectorDescriptors()[i]).divideSumDxBy(norm);
			(descriptor.getSectorDescriptors()[i]).divideSumAbsDxBy(norm);
			(descriptor.getSectorDescriptors()[i]).divideSumDyBy(norm);
			(descriptor.getSectorDescriptors()[i]).divideSumAbsDyBy(norm);
		}
	}

	private KeyPoint keyPoint;
	private KeyPointSectorDescriptor[] sectorDescriptors;

	public KeyPointDescriptor() {
		this.sectorDescriptors = new KeyPointSectorDescriptor[DESCRIPTOR_SECTOR_GRID_SIZE_2D];
		for (int i = 0; i < sectorDescriptors.length; i++) {
			sectorDescriptors[i] = new KeyPointSectorDescriptor();
		}
	}

	private void setKeyPoint(KeyPoint keyPoint) {
		this.keyPoint = keyPoint;
	}

	public KeyPoint getKeyPoint() {
		return keyPoint;
	}

	private KeyPointSectorDescriptor[] getSectorDescriptors() {
		return sectorDescriptors;
	}

	public KeyPointSectorDescriptor getSectorDescriptor(int index) {
		return sectorDescriptors[index];
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Descriptor[%s]", keyPoint);
	}
	
	
}
