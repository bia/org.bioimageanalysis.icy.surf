package org.bioimageanalysis.icy.surf.matching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.bioimageanalysis.icy.surf.KeyPoint;
import org.bioimageanalysis.icy.surf.KeyPointDescriptor;
import org.bioimageanalysis.icy.surf.KeyPointSectorDescriptor;
import org.bioimageanalysis.icy.surf.matching.ransac.RansacAffineTransformationCalculator;
import org.bioimageanalysis.icy.surf.matching.ransac.RansacAffineTransformationCalculatorException;

import com.google.common.util.concurrent.AtomicDouble;

public class SurfMatcher {

	private static double MATCH_RATIO = 0.6;

	private List<KeyPointDescriptor> descriptorList1;
	private List<KeyPointDescriptor> descriptorList2;

	private List<DescriptorMatch> matches;

	private double squaredMatchRatio;
	private int matchNumberLimit;

	private int ransacMaxNumIterations = RansacAffineTransformationCalculator.DEFAULT_MAX_NUM_ITERATIONS;
	private double ransacMaxErrorThreshold = RansacAffineTransformationCalculator.DEFAULT_SQUARED_ERROR_THRESHOLD;
	private int ransacMinNumInliers = RansacAffineTransformationCalculator.DEFAULT_MIN_NUM_INLIERS;

	public SurfMatcher() {}

	public List<KeyPointDescriptor> getDescriptorList1() {
		return descriptorList1;
	}

	public void setDescriptorList1(List<KeyPointDescriptor> descriptorList1) {
		this.descriptorList1 = descriptorList1;
	}

	public List<KeyPointDescriptor> getDescriptorList2() {
		return descriptorList2;
	}

	public void setDescriptorList2(List<KeyPointDescriptor> descriptorList2) {
		this.descriptorList2 = descriptorList2;
	}

	public int getMatchNumberLimit() {
		return matchNumberLimit;
	}

	public void setMatchNumberLimit(int limit) {
		this.matchNumberLimit = limit;
	}

	public void setRansacMaxNumIterations(int ransacMaxNumIterations) {
		this.ransacMaxNumIterations = ransacMaxNumIterations;
	}

	public void setRansacMaxErrorThreshold(double ransacMaxErrorThreshold) {
		this.ransacMaxErrorThreshold = ransacMaxErrorThreshold;
	}

	public void setRansacMinNumInliers(int ransacMinNumInliers) {
		this.ransacMinNumInliers = ransacMinNumInliers;
	}

	public void compute() throws InterruptedException, RuntimeException {
		createClosestDescriptorMatches();
		cleanIdenticalMatches();
		filterCorrelatedMatches();
	}

	private void createClosestDescriptorMatches() throws InterruptedException {
		squaredMatchRatio = MATCH_RATIO * MATCH_RATIO;

		PriorityQueue<DescriptorMatch> linkedMatches = new PriorityQueue<>(
				Comparator.comparing(DescriptorMatch::getMatchScore));

		// Matching is not symmetric.
		Collections.shuffle(getDescriptorList1());
		for (int i = 0; i < getDescriptorList1().size(); i++) {
			AtomicInteger position = new AtomicInteger(-1);
			AtomicDouble d1 = new AtomicDouble(3);
			AtomicDouble d2 = new AtomicDouble(3);

			for (int j = 0; j < getDescriptorList2().size(); j++) {
				if (Thread.interrupted())
					throw new InterruptedException();

				compute2ClosestDescriptors(i, j, position, d1, d2);
			}

			// Try to match it
			if (isAGoodMatch(position, d1, d2)) {
				linkedMatches.add(createMatch(i, position.get(), d1.get() - squaredMatchRatio * d2.get()));
			}
			// Found enough matches
			if (matchNumberLimit > 0 && linkedMatches.size() == matchNumberLimit)
				break;
		}
		matches = linkedMatches.stream().limit(matchNumberLimit).collect(Collectors.toList());
	}

	private void compute2ClosestDescriptors(int i, int j, AtomicInteger position, AtomicDouble d1, AtomicDouble d2) {
		double d = getEuclideanDistance(getDescriptorList1().get(i), getDescriptorList2().get(j));

		if (getDescriptorList1().get(i).getKeyPoint().isLaplacianSignPositive() == getDescriptorList2().get(j).getKeyPoint()
				.isLaplacianSignPositive()) {
			d2.set((d2.get() > d)? d: d2.get());
			if (d1.get() > d) {
				position.set(j);
				d2.set(d1.get());
				d1.set(d);
			}
		}
	}

	private double getEuclideanDistance(KeyPointDescriptor descriptor1, KeyPointDescriptor descriptor2) {
		double sum = 0;
		for (int i = 0; i < 16; i++) {
			KeyPointSectorDescriptor sectorDescriptor1 = descriptor1.getSectorDescriptor(i);
			KeyPointSectorDescriptor sectorDescriptor2 = descriptor2.getSectorDescriptor(i);
			sum += (sectorDescriptor1.getSumDx() - sectorDescriptor2.getSumDx())
					* (sectorDescriptor1.getSumDx() - sectorDescriptor2.getSumDx())
					+ (sectorDescriptor1.getSumDy() - sectorDescriptor2.getSumDy())
							* (sectorDescriptor1.getSumDy() - sectorDescriptor2.getSumDy())
					+ (sectorDescriptor1.getSumAbsDy() - sectorDescriptor2.getSumAbsDy())
							* (sectorDescriptor1.getSumAbsDy() - sectorDescriptor2.getSumAbsDy())
					+ (sectorDescriptor1.getSumAbsDx() - sectorDescriptor2.getSumAbsDx())
							* (sectorDescriptor1.getSumAbsDx() - sectorDescriptor2.getSumAbsDx());
		}
		return sum;
	}

	private boolean isAGoodMatch(AtomicInteger position, AtomicDouble d1, AtomicDouble d2) {
		return position.get() >= 0 && squaredMatchRatio * d2.get() > d1.get();
	}

	private DescriptorMatch createMatch(int i, int j, double score) {
		DescriptorMatch match = new DescriptorMatch();
		KeyPoint kp1 = getDescriptorList1().get(i).getKeyPoint();
		match.setX1(kp1.getX());
		match.setY1(kp1.getY());
		KeyPoint kp2 = getDescriptorList2().get(j).getKeyPoint();
		match.setX2(kp2.getX());
		match.setY2(kp2.getY());
		match.setMatchScore(score);
		return match;
	}

	private void cleanIdenticalMatches() {
		matches = new ArrayList<>(new HashSet<>(matches));
	}

	private void filterCorrelatedMatches() throws RuntimeException {
		try {
			RansacAffineTransformationCalculator calculator = new RansacAffineTransformationCalculator.Builder(matches)
					.setMaxNumIterations(ransacMaxNumIterations).setNumInliersThreshold(ransacMinNumInliers)
					.setErrorThreshold(ransacMaxErrorThreshold).build();
			calculator.compute();
			matches = calculator.getFilteredMatches();
		} catch (RansacAffineTransformationCalculatorException e) {
			//			e.printStackTrace();
			//			System.err.println("RANSAC failed to compute a correct affine transformation. Giving raw matches instead");
			throw new RuntimeException("RANSAC failed to compute a correct affine transformation.", e);
		} catch (IllegalArgumentException e) {
			//System.err.format("Insufficient matches to compute RANSAC (%d). Giving raw matches instead\n", matches.size());
			throw new RuntimeException(String.format("Insufficient matches to compute RANSAC (%d).", matches.size()), e);
		}
	}

	public List<DescriptorMatch> getMatches() {
		return matches;
	}
}
