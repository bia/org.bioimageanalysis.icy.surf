package org.bioimageanalysis.icy.surf.matching;

public class DescriptorMatch {
	private double x1, y1;
	private double x2, y2;
	private double matchScore;

	public double getX1() {
		return x1;
	}

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public double getY1() {
		return y1;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public double getX2() {
		return x2;
	}

	public void setX2(double x2) {
		this.x2 = x2;
	}

	public double getY2() {
		return y2;
	}

	public void setY2(double y2) {
		this.y2 = y2;
	}

	public double getMatchScore() {
		return matchScore;
	}

	public void setMatchScore(double matchScore) {
		this.matchScore = matchScore;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x1);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x2);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y1);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y2);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DescriptorMatch)) {
			return false;
		}
		DescriptorMatch other = (DescriptorMatch) obj;
		if (Double.doubleToLongBits(x1) != Double.doubleToLongBits(other.x1)) {
			return false;
		}
		if (Double.doubleToLongBits(x2) != Double.doubleToLongBits(other.x2)) {
			return false;
		}
		if (Double.doubleToLongBits(y1) != Double.doubleToLongBits(other.y1)) {
			return false;
		}
		if (Double.doubleToLongBits(y2) != Double.doubleToLongBits(other.y2)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("DescriptorMatch [x1=%s, y1=%s, x2=%s, y2=%s, matchScore=%s]", x1, y1, x2, y2, matchScore);
	}

}
