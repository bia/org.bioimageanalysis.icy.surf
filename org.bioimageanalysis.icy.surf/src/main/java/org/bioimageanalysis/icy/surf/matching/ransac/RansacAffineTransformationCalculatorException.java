package org.bioimageanalysis.icy.surf.matching.ransac;

@SuppressWarnings("serial")
public class RansacAffineTransformationCalculatorException extends RuntimeException {

	public RansacAffineTransformationCalculatorException() {}

	public RansacAffineTransformationCalculatorException(String message) {
		super(message);
	}

	public RansacAffineTransformationCalculatorException(Throwable cause) {
		super(cause);
	}

	public RansacAffineTransformationCalculatorException(String message, Throwable cause) {
		super(message, cause);
	}

}
