/*
 * Copyright 2010-2018 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package org.bioimageanalysis.icy.surf.extraction;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import org.bioimageanalysis.icy.surf.KeyPoint;
import org.bioimageanalysis.icy.surf.KeyPointDescriptor;
import org.bioimageanalysis.icy.surf.image.ArrayImage;
import org.bioimageanalysis.icy.surf.image.ArrayImageHelper;
import org.bioimageanalysis.icy.surf.image.ArrayImageIntegrator;
import org.bioimageanalysis.icy.surf.image.IcyBufferedImageHelper;

import icy.common.exception.UnsupportedFormatException;
import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.type.DataType;

/**
 * SURF feature calculator. This code is based on IPOL SURF solution.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class SurfFeatureCalculator {

	private static final int NUM_OCTAVES = 4;
	private static final int NUM_INTERVALS = 4;
	private static final int SAMPLING_STEP = 2;
	private static final int NUMBER_OF_SECTORS = 20;

	private IcyBufferedImage targetImage;
	private IcyBufferedImage grayImage;
	private ArrayImage integralImage;
	private ArrayImageIntegrator integrator;

	private int currentOctave;
	private int currentOctavePowerOf2;
	private int currentOctaveSamplingStep;
	private int currentOctaveImageWidth;
	private int currentOctaveImageHeight;
	private ArrayImage[] currentOctaveHessians;
	private ArrayImage[] currentOctaveLaplacianSigns;

	private int currentIntervalLValue;
	private int currentIntervalBound1;
	private int currentIntervalBound2;
	private int currentIntervalBound3;
	private int currentIntervalBound4;
	private int currentIntervalBound5;
	private double currentIntervalXXFrobeniusNorm;
	private double currentIntervalXYFrobeniusNorm;
	private double hessianThreshold;
	private double currentXUnsampled;
	private double currentYUnsampled;
	private double currentBoxSize;

	private List<KeyPoint> keyPoints;
	private List<KeyPointDescriptor> descriptors;

	private double minHessianValue;
	private double maxHessianValue;

	public SurfFeatureCalculator() {}

	public IcyBufferedImage getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(BufferedImage image) {
		targetImage = IcyBufferedImage.createFrom(image);
	}

	private void unsetTargetImage() {
		targetImage = null;
	}

	public double getHessianThreshold() {
		return hessianThreshold;
	}

	public void setHessianThreshold(double hessianThreshold) {
		this.hessianThreshold = hessianThreshold;
	}

	private void unsetHessianThreshold() {
		this.hessianThreshold = -1;
	}

	public void computeFeatures() throws UnsupportedFormatException, InterruptedException {
		if (getTargetImage() == null)
			throw new RuntimeException("No input image specified");
		if (getHessianThreshold() < 0)
			throw new RuntimeException("No hessian threshold specified");
		createGrayNormalizedImage();
		unsetTargetImage();
		createIntegralImage();
		findFeatures();
		unsetHessianThreshold();
	}

	private void createGrayNormalizedImage() {
		IcyBufferedImage baseImage = getTargetImage();
		BufferedImage grayBImage = new BufferedImage(baseImage.getWidth(), baseImage.getHeight(),
				BufferedImage.TYPE_BYTE_GRAY);
		grayImage = IcyBufferedImage.createFrom(IcyBufferedImageUtil.toBufferedImage(baseImage, grayBImage));
		grayImage.updateChannelsBounds();
		grayImage = IcyBufferedImageUtil.convertToType(grayImage, DataType.DOUBLE, true, true);
		grayImage.beginUpdate();
		for (int y = 0; y < grayImage.getHeight(); y++) {
			for (int x = 0; x < grayImage.getWidth(); x++) {
				grayImage.setData(x, y, 0, (int) Math.round(grayImage.getData(x, y, 0) * 255));
			}
		}
		grayImage.endUpdate();
		grayImage.updateChannelsBounds();
		setGrayImage(grayImage);
	}

	private void setGrayImage(IcyBufferedImage grayImage) {
		this.grayImage = grayImage;
	}

	public IcyBufferedImage getGrayImage() {
		return grayImage;
	}

	private void createIntegralImage() {
		integrator = new ArrayImageIntegrator();
		integrator.setTargetImage(ArrayImageHelper.getArrayImageFrom(getGrayImage()));
		integrator.setPaddingSize(312);
		integrator.compute();
		setIntegralImage(integrator.getIntegralImage());
	}

	private void setIntegralImage(ArrayImage integralImage) {
		this.integralImage = integralImage;
	}

	public ArrayImage getIntegralImage() {
		return integralImage;
	}

	private ArrayImageIntegrator getIntegrator() {
		return integrator;
	}

	private void findFeatures() throws InterruptedException {
		setKeyPoints(new LinkedList<>());
		maxHessianValue = Double.NEGATIVE_INFINITY;
		minHessianValue = Double.POSITIVE_INFINITY;
		for (int octave = 0; octave < NUM_OCTAVES; octave++) {
			currentOctave = octave;
			findOctaveFeatures();
		}

		computeDescriptors();
	}

	private void setKeyPoints(List<KeyPoint> keyPoints) {
		this.keyPoints = keyPoints;
	}

	public List<KeyPoint> getKeyPoints() {
		return keyPoints;
	}

	private void findOctaveFeatures() throws InterruptedException {
		currentOctavePowerOf2 = (int) Math.pow(2, currentOctave + 1);
		currentOctaveSamplingStep = (int) Math.pow(SAMPLING_STEP, currentOctave);
		currentOctaveImageWidth = IcyBufferedImageHelper.getImageWidthForSamplingStep(getGrayImage(),
				currentOctaveSamplingStep);
		currentOctaveImageHeight = IcyBufferedImageHelper.getImageHeightForSamplingStep(getGrayImage(),
				currentOctaveSamplingStep);

		initializeHessianArrays();
		for (int interval = 0; interval < NUM_INTERVALS; interval++) {
			if (Thread.interrupted())
				throw new InterruptedException();
			computeHessianForInterval(interval);
		}
		for (int interval = 1; interval < NUM_INTERVALS - 1; interval++) {
			detectKeyPointsAtInterval(interval);
		}

	}

	private void initializeHessianArrays() {
		this.currentOctaveHessians = new ArrayImage[NUM_INTERVALS];
		this.currentOctaveLaplacianSigns = new ArrayImage[NUM_INTERVALS];
	}

	private void computeHessianForInterval(int currentInterval) throws InterruptedException {
		this.currentOctaveHessians[currentInterval] = new ArrayImage(currentOctaveImageWidth, currentOctaveImageHeight);
		this.currentOctaveLaplacianSigns[currentInterval] = new ArrayImage(currentOctaveImageWidth,
				currentOctaveImageHeight);

		this.currentIntervalLValue = currentOctavePowerOf2 * (currentInterval + 1) + 1;

		this.currentIntervalBound1 = -currentIntervalLValue + 1;
		this.currentIntervalBound2 = 3 * currentIntervalLValue;
		this.currentIntervalBound3 = Math.floorDiv((-currentIntervalLValue + 1), 2);
		this.currentIntervalBound4 = Math.floorDiv((-currentIntervalLValue + 1), 2) - currentIntervalLValue;
		this.currentIntervalBound5 = 2 * currentIntervalLValue - 1;

		this.currentIntervalXXFrobeniusNorm = Math.sqrt(6 * currentIntervalLValue * (2 * currentIntervalLValue - 1));
		this.currentIntervalXYFrobeniusNorm = Math.sqrt(4 * currentIntervalLValue * currentIntervalLValue);

		for (int y = 0; y < currentOctaveImageHeight; y++) {
			int currentIntervalYPosition = y * currentOctaveSamplingStep;

			for (int x = 0; x < currentOctaveImageWidth; x++) {
				computeCurrentIntervalHessianForLine(currentInterval, x, y, currentIntervalYPosition);
			}
		}
	}

	private void computeCurrentIntervalHessianForLine(int currentInterval, int x, int y, int currentIntervalYPosition)
			throws InterruptedException {
		if (Thread.interrupted())
			throw new InterruptedException();

		int currentIntervalXPosition = x * currentOctaveSamplingStep;

		double dXX = getXXDerivativeAt(currentIntervalXPosition, currentIntervalYPosition);
		double dYY = getYYDerivativeAt(currentIntervalXPosition, currentIntervalYPosition);
		double dXY = getXYDerivativeAt(currentIntervalXPosition, currentIntervalYPosition);

		currentOctaveHessians[currentInterval].setValue(x, y, dXX * dYY - 0.8317 * (dXY * dXY));
		currentOctaveLaplacianSigns[currentInterval].setValue(x, y, ((dXX + dYY) > 0? 1: 0));
	}

	private double getXXDerivativeAt(int x, int y) {
		double dXX = integrator.getSquareConvolutionXYAt(x, y, currentIntervalBound1, currentIntervalBound4,
				currentIntervalBound5, currentIntervalBound2);
		dXX -= 3 * integrator.getSquareConvolutionXYAt(x, y, currentIntervalBound1, currentIntervalBound3,
				currentIntervalBound5, currentIntervalLValue);
		dXX /= currentIntervalXXFrobeniusNorm;
		return dXX;
	}

	private double getYYDerivativeAt(int x, int y) {
		double dYY = integrator.getSquareConvolutionXYAt(x, y, currentIntervalBound4, currentIntervalBound1,
				currentIntervalBound2, currentIntervalBound5);
		dYY -= 3 * integrator.getSquareConvolutionXYAt(x, y, currentIntervalBound3, currentIntervalBound1,
				currentIntervalLValue, currentIntervalBound5);
		dYY /= currentIntervalXXFrobeniusNorm;
		return dYY;
	}

	private double getXYDerivativeAt(int x, int y) {
		double dXY = integrator.getSquareConvolutionXYAt(x, y, 1, 1, currentIntervalLValue, currentIntervalLValue);
		dXY += integrator.getSquareConvolutionXYAt(x, y, 0, 0, -currentIntervalLValue, -currentIntervalLValue);
		dXY += integrator.getSquareConvolutionXYAt(x, y, 1, 0, currentIntervalLValue, -currentIntervalLValue);
		dXY += integrator.getSquareConvolutionXYAt(x, y, 0, 1, -currentIntervalLValue, currentIntervalLValue);
		dXY /= currentIntervalXYFrobeniusNorm;
		return dXY;
	}

	private void detectKeyPointsAtInterval(int currentInterval) {
		currentIntervalLValue = currentOctavePowerOf2 * (currentInterval + 1) + 1;

		for (int y = 1; y < currentOctaveImageHeight - 1; y++) {
			detectKeyPointsAtIntervalInLine(currentInterval, y);
		}
	}

	private void detectKeyPointsAtIntervalInLine(int currentInterval, int y) {
		for (int x = 1; x < currentOctaveImageWidth - 1; x++) {
			if (isHessianMaximum(x, y, currentInterval)) {
				currentXUnsampled = x * currentOctaveSamplingStep;
				currentYUnsampled = y * currentOctaveSamplingStep;
				currentBoxSize = (int) (0.4 * (currentOctavePowerOf2 * (currentInterval + 1) + 2));
				if (isPositionInterpolatedInScaleSpace(x, y, currentInterval)) {
					addKeyPoint(currentOctaveLaplacianSigns[currentInterval].getValueAt(x, y) > 0);
				}
			}
		}
	}

	private boolean isHessianMaximum(int x, int y, int currentInterval) {
		double currentHessianValue = currentOctaveHessians[currentInterval].getValueAt(x, y);
		minHessianValue = Math.min(minHessianValue, currentHessianValue);
		maxHessianValue = Math.max(maxHessianValue, currentHessianValue);
		if (currentHessianValue > hessianThreshold) {
			return isHessianMaximumLocally(x, y, currentInterval, currentHessianValue);
		}
		return false;
	}

	private boolean isHessianMaximumLocally(int x, int y, int currentInterval, double hessianValue) {
		for (int j = y - 1; j < 2 + y; j++) {
			for (int i = x - 1; i < 2 + x; i++) {
				if (currentOctaveHessians[currentInterval - 1].getValueAt(i, j) >= hessianValue)
					return false;
				if (currentOctaveHessians[currentInterval + 1].getValueAt(i, j) >= hessianValue)
					return false;
				if ((x != i || y != j) && currentOctaveHessians[currentInterval].getValueAt(i, j) >= hessianValue)
					return false;
			}
		}
		return true;
	}

	private boolean isPositionInterpolatedInScaleSpace(int x, int y, int currentInterval) {
		if (!isPositionInsideCurrentHessian(x, y, currentInterval))
			return false;

		double mx, my, mi, dx, dy, di, dxx, dyy, dii, dxy, dxi, dyi;

		dx = (currentOctaveHessians[currentInterval].getValueAt(x + 1, y)
				- currentOctaveHessians[currentInterval].getValueAt(x - 1, y)) / 2;
		dy = (currentOctaveHessians[currentInterval].getValueAt(x, y + 1)
				- currentOctaveHessians[currentInterval].getValueAt(x, y - 1)) / 2;
		di = (currentOctaveHessians[currentInterval].getValueAt(x, y)
				- currentOctaveHessians[currentInterval].getValueAt(x, y)) / 2;

		double a = currentOctaveHessians[currentInterval].getValueAt(x, y);
		dxx = currentOctaveHessians[currentInterval].getValueAt(x + 1, y)
				+ currentOctaveHessians[currentInterval].getValueAt(x - 1, y) - 2 * a;
		dyy = currentOctaveHessians[currentInterval].getValueAt(x, y + 1)
				+ currentOctaveHessians[currentInterval].getValueAt(x, y - 1) - 2 * a;
		dii = currentOctaveHessians[currentInterval - 1].getValueAt(x, y)
				+ currentOctaveHessians[currentInterval + 1].getValueAt(x, y) - 2 * a;

		dxy = (currentOctaveHessians[currentInterval].getValueAt(x + 1, y + 1)
				- currentOctaveHessians[currentInterval].getValueAt(x + 1, y - 1)
				- currentOctaveHessians[currentInterval].getValueAt(x - 1, y + 1)
				+ currentOctaveHessians[currentInterval].getValueAt(x - 1, y - 1)) / 4;
		dxi = (currentOctaveHessians[currentInterval + 1].getValueAt(x + 1, y)
				- currentOctaveHessians[currentInterval + 1].getValueAt(x - 1, y)
				- currentOctaveHessians[currentInterval - 1].getValueAt(x + 1, y)
				+ currentOctaveHessians[currentInterval - 1].getValueAt(x - 1, y)) / 4;
		dyi = (currentOctaveHessians[currentInterval + 1].getValueAt(x, y + 1)
				- currentOctaveHessians[currentInterval + 1].getValueAt(x, y - 1)
				- currentOctaveHessians[currentInterval - 1].getValueAt(x, y + 1)
				+ currentOctaveHessians[currentInterval - 1].getValueAt(x, y - 1)) / 4;

		double det = dxx * dyy * dii - dxx * dyi * dyi - dyy * dxi * dxi + 2 * dxi * dyi * dxy - dii * dxy * dxy;
		if (det != 0) {
			mx = -1 / det * (dx * (dyy * dii - dyi * dyi) + dy * (dxi * dyi - dii * dxy) + di * (dxy * dyi - dyy * dxi));
			my = -1 / det * (dx * (dxi * dyi - dii * dxy) + dy * (dxx * dii - dxi * dxi) + di * (dxy * dxi - dxx * dyi));
			mi = -1 / det * (dx * (dxy * dyi - dyy * dxi) + dy * (dxy * dxi - dxx * dyi) + di * (dxx * dyy - dxy * dxy));

			if (Math.abs(mx) < 1 && Math.abs(my) < 1 && Math.abs(mi) < 1) {
				currentXUnsampled = currentOctaveSamplingStep * (x + mx) + 0.5;
				currentYUnsampled = currentOctaveSamplingStep * (y + my) + 0.5;
				currentBoxSize = 0.4 * (1 + currentOctavePowerOf2 * (currentInterval + mi + 1));
				return true;
			}
		}
		return false;
	}

	private boolean isPositionInsideCurrentHessian(int x, int y, int currentInterval) {
		return x > 0 && y > 0 && x < currentOctaveHessians[currentInterval].getWidth() - 2
				&& y < currentOctaveHessians[currentInterval].getHeight() - 2;
	}

	private void addKeyPoint(boolean laplacianSignPositive) {
		KeyPoint point = new KeyPoint(currentXUnsampled, currentYUnsampled, currentBoxSize,
				getOrientation((int) currentXUnsampled, (int) currentYUnsampled, NUMBER_OF_SECTORS, currentBoxSize),
				laplacianSignPositive);
		getKeyPoints().add(point);
	}

	private double getOrientation(int x, int y, int numberOfSectors, double scale) {
		double[] haarResponseX = new double[numberOfSectors];
		double[] haarResponseY = new double[numberOfSectors];
		double[] haarResponseSectorX = new double[numberOfSectors];
		double[] haarResponseSectorY = new double[numberOfSectors];
		double answerX, answerY;
		double gauss;

		int theta;

		// Computation of the contribution of each angular sectors.
		for (int i = -6; i <= 6; i++) {
			for (int j = -6; j <= 6; j++) {
				if (i * i + j * j <= 36) {

					answerX = getIntegrator().getHaarX((int) (x + scale * i), (int) (y + scale * j), (int) Math.round(2 * scale));
					answerY = getIntegrator().getHaarY((int) (x + scale * i), (int) (y + scale * j), (int) Math.round(2 * scale));

					// Associated angle
					theta = (int) ((Math.atan2(answerY, answerX) * numberOfSectors) / (2 * Math.PI));
					theta = (theta >= 0)? theta: (theta + numberOfSectors);

					// Gaussian weight
					gauss = KeyPointDescriptor.getGaussian(i, j, 2);

					// Cumulative answers
					haarResponseSectorX[theta] += answerX * gauss;
					haarResponseSectorY[theta] += answerY * gauss;
				}
			}
		}

		// Compute a windowed answer
		for (int i = 0; i < numberOfSectors; i++) {
			for (int j = -Math.floorDiv(numberOfSectors, 12); j <= Math.floorDiv(numberOfSectors, 12); j++) {
				if (0 <= i + j && i + j < numberOfSectors) {
					haarResponseX[i] += haarResponseSectorX[i + j];
					haarResponseY[i] += haarResponseSectorY[i + j];
				}
				// The answer can be on any quadrant of the unit circle
				else if (i + j < 0) {
					haarResponseX[i] += haarResponseSectorX[i + j + numberOfSectors];
					haarResponseY[i] += haarResponseSectorY[i + j + numberOfSectors];
				}

				else {
					haarResponseX[i] += haarResponseSectorX[i + j - numberOfSectors];
					haarResponseY[i] += haarResponseSectorY[i + j - numberOfSectors];
				}
			}
		}

		// Find out the maximum answer
		int t = 0;
		double max = haarResponseX[0] * haarResponseX[0] + haarResponseY[0] * haarResponseY[0];
		for (int i = 1; i < numberOfSectors; i++) {
			double norm = haarResponseX[i] * haarResponseX[i] + haarResponseY[i] * haarResponseY[i];
			t = ((max < norm)? i: t);
			max = ((max < norm)? norm: max);
		}

		// Return the angle ; better than atan which is not defined in pi/2"
		double orientation = Math.atan2(haarResponseY[t], haarResponseX[t]);
		return orientation;
	}

	private void computeDescriptors() {
		setDescriptors(KeyPointDescriptor.createDescriptors(integrator, getKeyPoints()));
	}

	private void setDescriptors(List<KeyPointDescriptor> descriptors) {
		this.descriptors = descriptors;
	}

	public List<KeyPointDescriptor> getDescriptors() {
		return descriptors;
	}

	public double getMinHessianValue() {
		return minHessianValue;
	}

	public double getMaxHessianValue() {
		return maxHessianValue;
	}
}
