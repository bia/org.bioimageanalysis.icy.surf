package org.bioimageanalysis.icy.surf.image;

public class ArrayImagePadder {
	private int targetWidth;
	private int targetHeight;
	private ArrayImage targetImage;

	private int padding;

	private int paddedWidth;
	private int paddedHeight;
	private ArrayImage paddedImage;

	public ArrayImagePadder() {
	}

	public ArrayImage getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(ArrayImage image) {
		this.targetImage = image;
		this.targetWidth = targetImage.getWidth();
		this.targetHeight = targetImage.getHeight();
	}

	private void unsetTargetImage() {
		this.targetImage = null;
		this.targetWidth = -1;
		this.targetHeight = -1;
	}

	public int getPaddingSize() {
		return this.padding;
	}

	public void setPaddingSize(int padding) {
		this.padding = padding;
	}

	private void unsetPaddingSize() {
		this.padding = -1;
	}

	public synchronized void compute() {
		if (getTargetImage() == null)
			throw new RuntimeException("No target image specified");
		if (getPaddingSize() < 0)
			throw new RuntimeException("No padding size specified");

		paddedWidth = targetWidth + 2 * padding;
		paddedHeight = targetHeight + 2 * padding;
		paddedImage = new ArrayImage(paddedWidth, paddedHeight);
		fillPaddedImage();

		unsetTargetImage();
		unsetPaddingSize();
	}

	private void fillPaddedImage() {
		int targetYPosition;
		for (int paddedYPosition = 0; paddedYPosition < paddedHeight; paddedYPosition++) {
			targetYPosition = getYPositionAtTargetImage(paddedYPosition);
			fillPaddedImageLine(paddedYPosition, targetYPosition);
		}
	}

	private int getYPositionAtTargetImage(int paddedYPosition) {
		if (paddedYPosition < padding)
			return getMirroredPositionAtTargetImage(padding - paddedYPosition, targetHeight);
		else if (paddedYPosition < paddedHeight - padding)
			return paddedYPosition - padding;
		else
			return getMirroredPositionAtTargetImage((targetHeight + paddedHeight - padding - 2) - paddedYPosition,
					targetHeight);
	}

	private int getMirroredPositionAtTargetImage(int position, int length) {
		if (position < 0)
			return getMirroredPositionAtTargetImage(-position, length);
		else if (position < length)
			return position;
		else
			return getMirroredPositionAtTargetImage((2 * length) - 2 - position, length);
	}

	private void fillPaddedImageLine(int paddedYPosition, int targetYPosition) {
		int targetXPosition;
		for (int paddedXPosition = 0; paddedXPosition < paddedWidth; paddedXPosition++) {
			targetXPosition = getXPositionAtTargetImage(paddedXPosition);
			fillPaddedImagePixel(paddedXPosition, paddedYPosition, targetXPosition, targetYPosition);
		}
	}

	private int getXPositionAtTargetImage(int paddedXPosition) {
		if (paddedXPosition < padding)
			return getMirroredPositionAtTargetImage(padding - paddedXPosition, targetWidth);
		else if (paddedXPosition < paddedWidth - padding)
			return paddedXPosition - padding;
		else
			return getMirroredPositionAtTargetImage((targetWidth + paddedWidth - padding - 2) - paddedXPosition, targetWidth);
	}

	private void fillPaddedImagePixel(int paddedXPosition, int paddedYPosition, int targetXPosition,
			int targetYPosition) {
		double value;
		try {
			value = getTargetImage().getValueAt(targetXPosition, targetYPosition);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(
					String.format("Invalid target image position (%d, %d)", targetXPosition, targetYPosition));
		}
		try {
			paddedImage.setValue(paddedXPosition, paddedYPosition, value);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(
					String.format("Invalid padded image position (%d, %d)", paddedXPosition, paddedYPosition));
		}
	}

	public ArrayImage getPaddedImage() {
		return paddedImage;
	}
}
