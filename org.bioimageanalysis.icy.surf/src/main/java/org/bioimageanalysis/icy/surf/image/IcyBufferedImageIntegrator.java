package org.bioimageanalysis.icy.surf.image;

import com.google.common.util.concurrent.AtomicDouble;

import icy.image.IcyBufferedImage;

public class IcyBufferedImageIntegrator {

	private IcyBufferedImage targetImage;
	private int targetSizeC;
	private int padding = -1;

	private IcyBufferedImage paddedImage;
	private int paddedWidth;
	private int paddedHeight;

	private IcyBufferedImage integralImage;

	public IcyBufferedImageIntegrator() {
	}

	public int getPadding() {
		return padding;
	}

	public void setPaddingSize(int padding) {
		this.padding = padding;
	}

	private IcyBufferedImage getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(IcyBufferedImage image) {
		this.targetImage = image;
		this.targetSizeC = image.getSizeC();
	}

	private void unsetTargetImage() {
		this.targetImage = null;
	}

	public synchronized void compute() {
		if (getTargetImage() == null)
			throw new RuntimeException("No target image specified");
		if (getPadding() < 0)
			throw new RuntimeException("No padding specified");

		IcyBufferedImage paddedImage = padImage(getTargetImage(), getPadding());
		unsetTargetImage();
		setPaddedImage(paddedImage);

		integralImage = new IcyBufferedImage(paddedWidth, paddedHeight, targetSizeC, getPaddedImage().getDataType_());
		integralImage.beginUpdate();
		for (int c = 0; c < targetSizeC; c++) {
			computeChannelIntegral(c);
		}
		integralImage.endUpdate();
		unsetPaddedImage();
	}

	private IcyBufferedImage padImage(IcyBufferedImage image, int padding) {
		IcyBufferedImagePadder padder = new IcyBufferedImagePadder();
		padder.setTargetImage(image);
		padder.setPaddingSize(padding);
		padder.compute();
		return padder.getPaddedImage();
	}

	private void setPaddedImage(IcyBufferedImage paddedImage) {
		this.paddedImage = paddedImage;
		this.paddedWidth = paddedImage.getWidth();
		this.paddedHeight = paddedImage.getHeight();
	}

	private void unsetPaddedImage() {
		this.paddedImage = null;
	}

	private IcyBufferedImage getPaddedImage() {
		return this.paddedImage;
	}

	private void computeChannelIntegral(int c) {
		integralImage.setData(0, 0, c, paddedImage.getDataAsDouble(0, 0, c));
		for (int x = 1; x < paddedWidth; x++) {
			computeIntegralFirstRowPixelValues(c, x);
		}

		for (int y = 1; y < paddedHeight; y++) {
			computeLineIntegralPixelValues(c, y);
		}
	}

	private void computeIntegralFirstRowPixelValues(int c, int x) {
		integralImage.setData(x, 0, c, integralImage.getData(x - 1, 0, c) + getPaddedImage().getData(x, 0, c));
	}

	private void computeLineIntegralPixelValues(int c, int y) {
		AtomicDouble lineSum = new AtomicDouble();
		for (int x = 0; x < paddedWidth; x++) {
			computeIntegralPixelValues(c, x, y, lineSum);
		}
	}

	private void computeIntegralPixelValues(int c, int x, int y, AtomicDouble lineSum) {
		lineSum.addAndGet(getPaddedImage().getData(x, y, c));
		integralImage.setData(x, y, c, lineSum.get() + integralImage.getData(x, y - 1, c));
	}

	public IcyBufferedImage getIntegralImage() {
		return this.integralImage;
	}

	public double getSquareConvolutionXYAt(int x, int y, int c, int boundX0, int boundX1, int boundY0, int boundY1) {
		int a1 = x - boundX0;
		int a2 = y - boundX1;
		int b1 = a1 - boundY0;
		int b2 = a2 - boundY1;

		a1 += padding;
		a2 += padding;
		b1 += padding;
		b2 += padding;

		double result = 0;

		try {
			result += integralImage.getData(b1, b2, c);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d) channel %d", b1, b2, c));
		}
		try {
			result += integralImage.getData(a1, a2, c);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d) channel %d", a1, a2, c));
		}
		try {
			result -= integralImage.getData(b1, a2, c);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d) channel %d", b1, a2, c));
		}

		try {
			result -= integralImage.getData(a1, b2, c);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException(String.format("Cannot convolve at (%d, %d) channel %d", a1, b2, c));
		}

		return result;
	}

	public double getHaarX(int x, int y, int c, int lambda) {
		double result = -getSquareConvolutionXYAt(x, y, c, 1, -lambda - 1, -lambda - 1, 2 * lambda + 1);
		result -= getSquareConvolutionXYAt(x, y, c, 0, -lambda - 1, lambda + 1, 2 * lambda + 1);
		return result;
	}

	public double getHaarY(int x, int y, int c, int lambda) {
		double result = 0;
		result -= getSquareConvolutionXYAt(x, y, c, -lambda - 1, 1, 2 * lambda + 1, -lambda - 1);
		result -= getSquareConvolutionXYAt(x, y, c, -lambda - 1, 0, 2 * lambda + 1, lambda + 1);
		return result;
	}

}
