package org.bioimageanalysis.icy.surf.matching.ransac;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.RealVector;
import org.bioimageanalysis.icy.surf.matching.DescriptorMatch;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RansacAffineTransformCalculatorSpec {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp() throws Exception {}

	@Test
	public void whenBuilderSetupWithNullMatchesThenIllegalArgumentException() {
		thrown.expect(IllegalArgumentException.class);
		new RansacAffineTransformationCalculator.Builder(null);
	}

	@Test
	public void whenBuilderSetupWithEmptyMatchesThenIllegalArgumentException() {
		thrown.expect(IllegalArgumentException.class);
		new RansacAffineTransformationCalculator.Builder(new ArrayList<>());
	}

	private AffineTransform affineTransformation;
	private List<DescriptorMatch> matches;

	@Test
	public void whenComputeWithLessThan6MatchesThenRansacAffineTransformationCalculatorException() {
		setTranslationAffineTransformation();
		createMatches(5, 1d);
		RansacAffineTransformationCalculator calculator = new RansacAffineTransformationCalculator.Builder(matches).build();
		thrown.expect(RansacAffineTransformationCalculatorException.class);
		calculator.compute();
	}

	private void setTranslationAffineTransformation() {
		affineTransformation = new AffineTransform();
		affineTransformation.translate(3d, 6.5d);
	}

	private void createMatches(int numMatches, double noise) {
		matches = new ArrayList<>(numMatches);
		for (int i = 0; i < numMatches; i++) {
			matches.add(createMatch(noise));
		}
	}

	private Random randomGenerator = new Random();

	private DescriptorMatch createMatch(double noise) {
		Point2D p1 = new Point2D.Double(randomGenerator.nextInt(1000), randomGenerator.nextInt(1000));
		Point2D p2 = new Point2D.Double();
		affineTransformation.transform(p1, p2);
		DescriptorMatch descriptor = new DescriptorMatch();
		descriptor.setX1(p1.getX());
		descriptor.setY1(p1.getY());
		descriptor.setX2(p2.getX() + randomGenerator.nextDouble() * noise);
		descriptor.setY2(p2.getY() + randomGenerator.nextDouble() * noise);
		return descriptor;
	}

	@Test
	public void whenComputeWithMoreThan6MatchesThenRansacAffineTransformationCloseToOriginalTransformation() {
		setTranslationAffineTransformation();
		createMatches(8, 0d);
		RansacAffineTransformationCalculator calculator = new RansacAffineTransformationCalculator.Builder(matches).build();
		calculator.compute();
		RealVector calculatorResult = calculator.getBestAffineTransformationParameters();
		double[] affineMatrix = new double[6];
		affineTransformation.getMatrix(affineMatrix);
		assertThat(calculatorResult.getEntry(0), is(closeTo(affineMatrix[0], 1e-10)));
		assertThat(calculatorResult.getEntry(1), is(closeTo(affineMatrix[2], 1e-10)));
		assertThat(calculatorResult.getEntry(2), is(closeTo(affineMatrix[4], 1e-10)));
		assertThat(calculatorResult.getEntry(3), is(closeTo(affineMatrix[1], 1e-10)));
		assertThat(calculatorResult.getEntry(4), is(closeTo(affineMatrix[3], 1e-10)));
		assertThat(calculatorResult.getEntry(5), is(closeTo(affineMatrix[5], 1e-10)));
	}

	@Test
	public void whenComputeWithMoreThan6NoisyMatchesThenRansacAffineTransformationCloseToOriginalTransformation() {
		setTranslationAffineTransformation();
		createMatches(20, 1d);
		RansacAffineTransformationCalculator calculator = new RansacAffineTransformationCalculator.Builder(matches)
				.setNumInliersThreshold(5).setErrorThreshold(.1).build();
		calculator.compute();
		RealVector calculatorResult = calculator.getBestAffineTransformationParameters();
		double[] affineMatrix = new double[6];
		affineTransformation.getMatrix(affineMatrix);
		assertThat(calculatorResult.getEntry(0), is(closeTo(affineMatrix[0], 2d)));
		assertThat(calculatorResult.getEntry(1), is(closeTo(affineMatrix[2], 2d)));
		assertThat(calculatorResult.getEntry(2), is(closeTo(affineMatrix[4], 2d)));
		assertThat(calculatorResult.getEntry(3), is(closeTo(affineMatrix[1], 2d)));
		assertThat(calculatorResult.getEntry(4), is(closeTo(affineMatrix[3], 2d)));
		assertThat(calculatorResult.getEntry(5), is(closeTo(affineMatrix[5], 2d)));
	}

}
