# SURF

<!-- badges: start -->
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Twitter](https://img.shields.io/twitter/follow/Icy_BioImaging?style=social)](https://twitter.com/Icy_BioImaging)
[![Image.sc forum](https://img.shields.io/badge/discourse-forum-brightgreen.svg?style=flat)](https://forum.image.sc/tag/icy)
<!-- badges: end -->

This is the repository for the source code of *SURF*, a plugin for the [bioimage analysis software Icy](http://icy.bioimageanalysis.org/), which was developed by members or former members of the [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). This plugin is licensed under GPL3 license.     
Icy is developed and maintained by [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). The [source code of Icy](https://gitlab.pasteur.fr/bia/icy) is also licensed under a GPL3 license.     



## Plugin description

<!-- Short description of goals of package, with descriptive links to the documentation website --> 

SURF Feature Matching        
A more detailed user documentation can be found on the SURF documentation page on the Icy website: http://icy.bioimageanalysis.org/plugin/surf-feature-matching/               


## Installation instructions

For end-users, refer to the documentation on the Icy website on [how to install an Icy plugin](http://icy.bioimageanalysis.org/tutorial/how-to-install-an-icy-plugin/).      

For developers, see our [Contributing guidelines](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CONTRIBUTING.md) and [Code of Conduct](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CODE-OF-CONDUCT.md).      

<!--  Here we should have some explanations on how to fork this repo (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Add any info related to Maven etc. How the project is build (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Any additional setup required (authentication tokens, etc).  -->


## Main functions and usage

<!-- list main functions, explain architecture, classname, give info on how to get started with the plugin. If applicable, how the package compares to other similar packages and/or how it relates to other packages -->

Classname: `plugins.danyfel80.registration.surf.MatchSurfFeatures`



## Citation 

Please cite this plugins as follows:          


Please also cite the Icy software and mention the version of Icy you used (bottom right corner of the GUI or first lines of the Output tab):     
de Chaumont, F. et al. (2012) Icy: an open bioimage informatics platform for extended reproducible research, [Nature Methods](https://www.nature.com/articles/nmeth.2075), 9, pp. 690-696       
http://icy.bioimageanalysis.org    



## Author(s)      

Daniel Felipe González Obando


## Additional information






This repository contains the code used for the plugin Match SURF Features and Extract SURF Features.

This two plugins are available as both Interface plugins and Protocols blocks.

The project has been prepared using gradle and eclipse to simplify project configuration. See below to setup this project on your machine.

 Installation

 Requirements

In order to be able to work with this project you must have installed the following software:

- **Icy**, version 1.9.5.1 or above. ( [Available here](http://icy.bioimageanalysis.org) )
  - The following plugins should be already installed in order to use the SURF project:
    - EzPlug SDK
    - Protocols SDK
- **Eclipse**, version _Neon_ or above. Make sure to have the _Buildship_ plugin installed. ([Available here](http://www.eclipse.org/downloads/))
- **Icy4Eclipse** plugin for Eclipse, the latest version available. Follow [these](http://icy.bioimageanalysis.org/index.php?display=startDevWithIcy) instructions.

 Setup

1. Use your *Git* repository manager of preference to download this repository (even Eclipse can do this). The repository URL is [https://gitlab.pasteur.fr/bia/org.bioimageanalysis.icy.surf.git](https://gitlab.pasteur.fr/bia/org.bioimageanalysis.icy.surf.git).
2. Make sure the environment variable **ICY_HOME** is set to the location of your Icy installation. _**Note**: This could be tricky on Mac so make sure to follow [these](https://stackoverflow.com/questions/829749/launch-mac-eclipse-with-environment-variables-set) instructions._
3. Open Eclipse and select the menu *File > Import...* Then select *Gradle > Existing Gradle Project*. Click *Next* the project root directory is demanded select the folder **org.bioimageanalysis.icy.surf** inside the folder you downloaded the at. Finally, click *Finish* to create the project in eclipse.

Eclipse will download the dependencies specified in the *gradle.build* file. When it finishes you should see the project without any problem on the project explorer of Eclipse. *If this is not the case, check that the environment variable ICY_HOME is correctly defined.*